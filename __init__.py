from fastapi.middleware.cors import CORSMiddleware
# import uvicorn


# def port_app(app):
#     if __name__ == "__main__":
#         uvicorn.run(app, host="0.0.0.0", port=8010)



def cros_app(app):
    origins = [
    
  "*"
    ]

    app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
    )
