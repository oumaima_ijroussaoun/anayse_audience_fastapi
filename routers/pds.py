from fastapi import APIRouter, Depends,HTTPException,status
from datetime import date, datetime, timedelta
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from typing import List
from crud import pds




routePDS = APIRouter(prefix="/API",tags=["API"])

############### Get Foyers Infos #################
@routePDS.get("/get/FoyersInfos/")
def get_foyers_pds(date_debut:date,date_fin:date):
    
    
    return pds.get_foyers_pds_entre_dates(date_debut,date_fin)




################################################################# PDS 4 CHAINES  #############################################################""
############### Get Foyers Infos 4Chaines #################
@routePDS.get("/get/Foyers_PDS_4Chaines/")
def get_foyers_pds_4chaines_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.get_foyers_pds_4chaines_entre_dates(date_debut,date_fin)


############### Get Individus Infos 4Chaines #################
@routePDS.get("/get/Individus_PDS_4Chaines/")
def individus_pds_4chaines_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.individus_pds_4chaines_entre_dates(date_debut,date_fin)


############### Get Invites Infos 4Chaines #################
@routePDS.get("/get/Invites_PDS_4Chaines/")
def invites_pds_4chaines_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.invites_pds_4chaines_entre_dates(date_debut,date_fin)

############### Get Postes Infos #################
@routePDS.get("/get/Postes_PDS_4Chaines/")
def postes_pds_4chaines_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.postes_pds_4chaines_entre_dates(date_debut,date_fin)


################################################################################################



################################################################# PDS FP  #############################################################""
############### Get Foyers Infos 4Chaines #################
@routePDS.get("/get/Foyers_PDS_FP/")
def get_foyers_pds_FP_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.get_foyers_pds_FP_entre_dates(date_debut,date_fin)


############### Get Individus Infos 4Chaines #################
@routePDS.get("/get/Individus_PDS_FP/")
def individus_pds_FP_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.individus_pds_FP_entre_dates(date_debut,date_fin)


############### Get Invites Infos 4Chaines #################
@routePDS.get("/get/Invites_PDS_FP/")
def invites_pds_FP_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.invites_pds_FP_entre_dates(date_debut,date_fin)

############### Get Postes Infos #################
@routePDS.get("/get/Postes_PDS_FP/")
def postes_pds_FP_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.postes_pds_FP_entre_dates(date_debut,date_fin)


################################################################################################


################################################################# PDS FP  #############################################################""
############### Get Foyers Infos 4Chaines #################
@routePDS.get("/get/Infos_4Chaines/")
def get_infos_4Chaines(date_debut:date,date_fin:date):
    
    
    return pds.get_infos_4Chaines(date_debut,date_fin)


#############################################################"
@routePDS.get("/get/Infos_FP/")
def get_infos_FP(date_debut:date,date_fin:date):
    
    
    return pds.get_infos_FP(date_debut,date_fin)




################################################################################################





############### Get Individus Infos #################
@routePDS.get("/get/IndividusInfos/")
def get_individus_pds(date_debut:date,date_fin:date):
    
    
    return pds.individus_pds_entre_dates(date_debut,date_fin)

############### Get Invites Infos #################
@routePDS.get("/get/InvitesInfos/")
def invites_pds(date_debut:date,date_fin:date):
    
    
    return pds.invites_pds_entre_dates(date_debut,date_fin)

############### Get All Indiv #################
@routePDS.get("/get/All_Indiv/")
def get_All_Indiv(date_debut:date,date_fin:date):
    
    
    return pds.get_All_Indiv(date_debut,date_fin)

############### Get Foyers Indiv #################
@routePDS.get("/get/Foyers_Indiv/")
def get_Foyers_Indiv(date_debut:date,date_fin:date):
    
    
    return pds.get_Foyers_Indiv(date_debut,date_fin)

############### Get Postes Infos #################
@routePDS.get("/get/PostesInfos/")
def postes_pds_entre_dates(date_debut:date,date_fin:date):
    
    
    return pds.postes_pds_entre_dates(date_debut,date_fin)

###############  Cumul sessions des postes #################
@routePDS.get("/get/Cumul_Postes/")
def get_cumul_moyenne_sessions_postes(date_debut:date,date_fin:date):
    
    
    return pds.get_cumul_moyenne_sessions_postes(date_debut,date_fin)

###############  Pourcentage d'audience des postes par rapport à l'audience foyer globale #################   
@routePDS.get("/get/Pourcentage_aud_postes/")
def pourcentage_audience_postes(date_debut:date,date_fin:date):
    
    
    return pds.pourcentage_audience_postes(date_debut,date_fin)

###############  cumul audience foyer #################
@routePDS.get("/get/Cumul_audience_foyers/")
def cumul_audience_foyer(date_debut:date,date_fin:date):
    
    
    return pds.cumul_audience_foyer(date_debut,date_fin)


###############  evolution audience par date #################
@routePDS.get("/get/Evolution_audience_par_date/")
def evolution_audience_par_date(date_debut:date,date_fin:date):
    
    
    return pds.evolution_audience_par_date(date_debut,date_fin)

############### Get All Infos #################
@routePDS.get("/get/AllInfos/")
def get_all_infos_pds():
    
    
    return pds.get_all_infos_pds()

############### Get Anomalie : Individus repondant qui n'ont pas de tickets #################
@routePDS.get("/get/Indiv_rep_no_tickets/")
def get_anomalie_indiv_repond_no_tickets(date_debut:date,date_fin:date):
    
    
    return pds.get_anomalie_indiv_repond_no_tickets(date_debut,date_fin)


############### Get Cumul et Moyenne des sessions des individus #################
@routePDS.get("/get/Cumul_Moyenne_Sessions_Indiv/")
def get_pourcentage_audience_touches(date_debut:date,date_fin:date):
    
    
    return pds.get_pourcentage_audience_touches(date_debut,date_fin)


############### Get Anomalie :Individus ayant un cumul d'audience =24h  #################
@routePDS.get("/get/Cumul_Audience_24h_Indiv/")
def get_anomalie_indiv_24h(date_debut:date,date_fin:date):
    
    
    return pds.get_ano_24h_indiv(date_debut,date_fin)

############### Get Anomalie :Individus ayant meme audience  #################
@routePDS.get("/get/Indiv_meme_audience/")
def get_ano_meme_aud_indiv(date_debut:date,date_fin:date):
    
    
    return pds.get_ano_meme_aud_indiv(date_debut,date_fin)

############### Get Anomalie :Individus ayant meme debut declaration  #################
@routePDS.get("/get/Indiv_meme_debut_declaration/")
def get_ano_meme_debut_declaration(date_debut:date,date_fin:date):
    
    
    return pds.get_ano_meme_debut_declaration(date_debut,date_fin)


############### Get toutes anomalies #################
@routePDS.get("/get/Indiv_toutes_anomalies/")
def toutes_anomalies_indiv(date_debut:date,date_fin:date):
    
    
    return pds.toutes_anomalies_indiv(date_debut,date_fin)


############### Get Foyers declarant touche 1 #################
@routePDS.get("/get/Foyers_Declarant_Touche_1/")
def get_foyers_declarant_touche_1(date_debut:date,date_fin:date):
    
    
    return pds.get_foyers_declarant_touche_1(date_debut,date_fin)
