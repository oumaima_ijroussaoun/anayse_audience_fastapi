from fastapi import APIRouter, Depends,HTTPException,status
from datetime import date, datetime, timedelta
from fastapi.security import OAuth2PasswordBearer,OAuth2PasswordRequestForm
from typing import List
from crud import triple_S




routeTriple_S = APIRouter(prefix="/API",tags=["API"])



############### Get Oublic foyer from "foyer" table #################
@routeTriple_S.get("/get/Infos_Foyers/")
def get_all_infos_foyers(date_debut:date):
    
    
    return triple_S.get_all_infos_foyers(date_debut)


############### Get Summary of Foyers #################
@routeTriple_S.get("/get/Summary_Foyers/")
def get_summary_foyers(date_debut:date):
    
    
    return triple_S.get_summary_foyers(date_debut)

############### Get Oublic foyer from "foyer" table #################
@routeTriple_S.get("/get/Rapport_Foyers/")
def get_report_foyers(date_debut:date):
    
    
    return triple_S.get_report_foyers(date_debut)

############### Get Oublic foyer from "foyer" table #################
@routeTriple_S.get("/get/Infos_Individus/")
def get_all_infos_individus(date_debut:date):
    
    
    return triple_S.get_all_infos_individus(date_debut)


############### Get Summary of Foyers #################
@routeTriple_S.get("/get/Summary_Individus/")
def get_summary_individus(date_debut:date):
    
    
    return triple_S.get_summary_individus(date_debut)

############### Get Oublic foyer from "foyer" table #################
@routeTriple_S.get("/get/Rapport_Individus/")
def get_report_individus(date_debut:date):
    
    
    return triple_S.get_report_individus(date_debut)
