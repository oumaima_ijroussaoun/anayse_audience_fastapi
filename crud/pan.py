from datetime import datetime ,date
import datetime
from datetime import datetime
from sqlalchemy.orm import Session, query, session 
from sqlalchemy import column, func 
import pandas as pd
from sqlalchemy import desc
import json
import locale
from typing import List
from sqlalchemy.sql.expression import  case, or_, true,and_
from sqlalchemy.sql.sqltypes import String
import requests
from sqlalchemy import func
from typing import Dict
import pandas as pd
from pyhocon import ConfigFactory
import time
from datetime import datetime,timedelta
import os
import sqlalchemy as sql
import psycopg2
import paramiko
import numpy as np



configFile = ConfigFactory.parse_file('config/config_pan.conf')
Foyers = configFile.get('Foyers')
Individus = configFile.get('Individus')



########################## Foyers entre dates #####################################
def get_foyers_pan_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')

    infosFoyer = []
    for date in dates:
        chemin_fichier = os.path.join('input/2022', f'{date.strftime("%Y%m%d")}.pan')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[0] == "1": #Foyer Infos
                    ligne = ligne.strip()  
                    foyer_infos = {}  
                    for cle, value in Foyers.items():
                        index = value["index"]
                        longueur = value["longueur"]
                        info = ligne[index : index  + longueur]
                        foyer_infos[cle] = info
                    infosFoyer.append(foyer_infos)

    df_foyers = pd.DataFrame(infosFoyer)
    df_foyers=df_foyers[["ID_FOYER","Poids_redressement","NPERF","CSP_CHEF","LIEU_RESIDENCE","HABITATION","LIEU_RESIDENCExHABITATION","Type_offre","CSPfoy","Type_offre_Milieu","REGION"]]
    df_foyers['DatePan']=date_debut
    df_foyers.rename(columns={'ID_FOYER':'f_id_mediamat'}, inplace=True)
    df_foyers['f_id_mediamat'] = df_foyers['f_id_mediamat'].astype(int)

    df_foyers=df_foyers.to_dict("records")
    return df_foyers





################################################ Individus entre dates #################################################
def individus_pan_entre_dates(date_debut, date_fin):
    dates = pd.date_range(date_debut, date_fin, freq='D')
    infosIndividu = []
    for date in dates:
        chemin_fichier = os.path.join('input/2022', f'{date.strftime("%Y%m%d")}.pan')
        if not os.path.exists(chemin_fichier):
            print(f'Le fichier {chemin_fichier} n\'existe pas.')
            continue

        with open(chemin_fichier, 'r') as fichier:
            for ligne in fichier:
                if ligne[0] == "2": #Individu Infos
                    ligne = ligne.strip()  
                    individus_infos = {}  
                    for cle, value in Individus.items():
                        index = value["index"]
                        longueur = value["longueur"]
                        info = ligne[index : index  + longueur]
                        individus_infos[cle] = info
                    infosIndividu.append(individus_infos)            
    df_individus = pd.DataFrame(infosIndividu)
    df_individus=df_individus[["ID_FOYER","NUM_INDIVIDU","Poids_redressement","AGE","AGE1","AGE2","AGE4","SEXEXAGE","SEXEXAGE3","ACTIF","SEXEXACTIFS","INSTRUCTION","SEXE_INSTRUCTION","AGE_CLAIR","Niveau_Diplome"]]
    df_individus.rename(columns={'ID_FOYER':'f_id_mediamat'}, inplace=True)
    df_individus['f_id_mediamat'] = df_individus['f_id_mediamat'].astype(int)
    df_individus=df_individus.to_dict("records")
    return df_individus








def get_house_hold_triple_s():
    df_triple_S=pd.read_sql("select house_hold_triple_s.f_id_mediamat,house_hold_triple_s.f_pres_enf,house_hold_triple_s.activite_menagere,house_hold_triple_s.langue_prin,house_hold_triple_s.nbr_tv,house_hold_triple_s.niv_diplome_cm from house_hold_triple_s ",con)
    return df_triple_S

def get_public_foyer():
    # Retrieve data from the database as a DataFrame
    df_public_foyer = pd.read_sql("select f_id_mediamat, f_num_con, f_nom, f_etat, f_csp, num_recrut, date_recrut, vague_recrut, f_date_deb, f_date_fin, motif_sorti, detail_motif_sorti, mode_trait_f, telephones, tv_online, nom_installateur from foyer", con)

    # Convert Timestamp columns to string representation
    df_public_foyer['date_recrut'] = df_public_foyer['date_recrut'].astype(str)
    df_public_foyer['f_date_deb'] = df_public_foyer['f_date_deb'].astype(str)
    df_public_foyer['f_date_fin'] = df_public_foyer['f_date_fin'].astype(str)
    df_public_foyer = df_public_foyer.fillna('')
    # Convert DataFrame to JSON
    #json_data = df_public_foyer.to_json(orient='records')
    return df_public_foyer



def create_connection (connection_string):
    print("-- Connecting to database")
    try:
        
        sql_engine = sql.create_engine(connection_string)
        print("Connected to database successfully !")
        return sql_engine

    except Exception as e:
        print("Connection to database failed, check the database status as well as your network configuration")
        print (e)


db_postgre="postgresql://postgres:Hive2021@192.168.55.6:5432/panda_2"
con=create_connection(db_postgre)


def get_foyers_triple_s():
        foyers=pd.read_sql("select  house_hold_triple_s.f_id_mediamat, house_hold_triple_s.f_pres_enf,house_hold_triple_s.csp_cm , house_hold_triple_s.activite_menagere , house_hold_triple_s.langue_prin,house_hold_triple_s.nbr_tv,house_hold_triple_s.niv_diplome_cm ,house_hold_triple_s.taille_milieu ,foyer.f_num_con, foyer.f_csp, foyer.date_recrut, foyer.f_date_deb, foyer.tv_online, foyer.nom_installateur,chef_menage.tranche_age from house_hold_triple_s left join foyer on house_hold_triple_s.f_id_mediamat=foyer.f_id_mediamat  left join chef_menage on foyer.f_num_con=chef_menage.f_num_con", con)
        foyers = foyers.fillna('')
        foyers=foyers.to_dict('records')
        return foyers


def get_age_chef_menage():
    foyers=pd.read_sql("select  member_triple_s.id_indiv,f_id_mediamat,age from public.member_triple_s join individu on CAST(member_triple_s.id_indiv AS INT)=individu.id_indiv  where is_cm=true", con)
    return foyers



def match_modality(value):
    if value>=15 and value<=34:
        return '1'
    elif value>=35 and value<=44:
        return '2'
    elif value>=45 and value<=54:
        return '4'
    elif value>=55 and value<=64:
        return '5'
    elif value>=65:
        return '6'






def create_pan_csv(date_debut,date_fin):

    df_foyers=pd.DataFrame(get_foyers_pan_entre_dates(date_debut,date_fin))
    df_individus=pd.DataFrame(individus_pan_entre_dates(date_debut,date_fin))


    foyers=pd.read_sql("select  house_hold_triple_s.f_id_mediamat, house_hold_triple_s.f_pres_enf,house_hold_triple_s.csp_cm , house_hold_triple_s.activite_menagere , house_hold_triple_s.langue_prin,house_hold_triple_s.nbr_tv,house_hold_triple_s.niv_diplome_cm ,house_hold_triple_s.taille_milieu ,foyer.f_num_con, foyer.f_csp, foyer.date_recrut, foyer.f_date_deb, foyer.tv_online, foyer.nom_installateur from house_hold_triple_s left join foyer on house_hold_triple_s.f_id_mediamat=foyer.f_id_mediamat  	 ", con)
    merged_df=df_foyers.merge(df_individus,on=['f_id_mediamat'], how='inner')
    merged_df=merged_df.merge(foyers,on=['f_id_mediamat'], how='inner')
    merged_df=merged_df.merge(get_age_chef_menage(),on=['f_id_mediamat'], how='inner')
    date = date_debut.strftime("%Y%m%d")
    merged_df['DatePan']=date
    pd.set_option('display.max_columns',None)
    merged_df.rename(columns={'age':'F_Age_Chef_Menage','taille_milieu':'F_Taille_Commune_Milieu','f_id_mediamat':'Foyer_ID','f_date_deb':'Date_Installation','Poids_redressement_x':'Poids_Foyer','NUM_INDIVIDU':'individu_ID','Poids_redressement_y':'Poids_Individu','NPERF':'F_NPERF','CSP_CHEF':'F_CSP_CHEF','LIEU_RESIDENCE':'F_LIEURESIDENCE','HABITATION':'F_HABITATION','LIEU_RESIDENCExHABITATION':'F_LIEURESIDENCExHABITATION','Type_offre':'F_Type_offre','CSPfoy':'F_CSPfoy','Type_offre_Milieu':'F_Type_offre_Milieu','REGION':'F_REGION','f_pres_enf':'F_Presence_Enfants_Foyer','activite_menagere':'F_Activite_Menagere_Hors_Foyer','langue_prin':'F_Langue_Parlee','nbr_tv':'F_Nb_TV_Foyer','niv_diplome_cm':'F_Niveau_DiplomeCM','AGE':'I_AGE','AGE1':'I_AGE1','AGE2':'I_AGE2','AGE4':'I_AGE4','SEXEXAGE':'I_SEXEXAGE','SEXEXAGE3':'I_SEXEXAGE3','ACTIF':'I_ACTIF','SEXEXACTIFS':'I_SEXEXACTIFS','INSTRUCTION':'I_INSTRUCTION','SEXE_INSTRUCTION':'I_SEXE_INSTRUCTION','AGE_CLAIR':'I_AGE_CLAIR','Niveau_Diplome':'I_Niveau_Diplome'}, inplace=True)
    merged_df=merged_df.drop(['f_num_con','date_recrut','tv_online','nom_installateur',"f_csp"], axis=1)
    new_order=['DatePan','Foyer_ID','Date_Installation','Poids_Foyer','individu_ID','Poids_Individu','F_NPERF','F_CSP_CHEF','F_LIEURESIDENCE','F_HABITATION','F_LIEURESIDENCExHABITATION','F_Type_offre','F_CSPfoy','F_Type_offre_Milieu','F_REGION','F_Age_Chef_Menage','F_Presence_Enfants_Foyer','F_Activite_Menagere_Hors_Foyer','F_Langue_Parlee','F_Nb_TV_Foyer','F_Niveau_DiplomeCM','F_Taille_Commune_Milieu','I_AGE','I_AGE1','I_AGE2','I_AGE4','I_SEXEXAGE','I_SEXEXAGE3','I_ACTIF','I_SEXEXACTIFS','I_INSTRUCTION','I_SEXE_INSTRUCTION','I_AGE_CLAIR','I_Niveau_Diplome']

    merged_df=merged_df[new_order]
    merged_df["individu_ID"]=merged_df['Foyer_ID'].astype(str) +merged_df['individu_ID']
    #merged_df["F_Age_Chef_Menage"]=merged_df['F_Age_Chef_Menage'].astype(int)
    merged_df=merged_df.sort_values(['Foyer_ID','individu_ID'],ascending=True)
    merged_df['Date_Installation'] = pd.to_datetime(merged_df['Date_Installation'])
    merged_df['Date_Installation'] = merged_df['Date_Installation'].dt.strftime('%d/%m/%y')
    merged_df['Poids_Individu'] = merged_df['Poids_Individu'].apply(lambda x: '0' if x == '0000' else x.lstrip('0'))
    merged_df['F_REGION'] = merged_df['F_REGION'].apply(lambda x: x.lstrip('0'))
    merged_df['I_AGE_CLAIR'] = merged_df['I_AGE_CLAIR'].apply(lambda x: x.lstrip('0'))
    merged_df['Poids_Foyer'] = merged_df['Poids_Foyer'].apply(lambda x: '0' if x == '0000' else x.lstrip('0'))
    merged_df['F_Age_Chef_Menage'] = merged_df['F_Age_Chef_Menage'].astype(int)
    merged_df['F_Age_Chef_Menage'] = merged_df['F_Age_Chef_Menage'].apply(match_modality)
    merged_df=merged_df.to_dict('records')

    return merged_df
