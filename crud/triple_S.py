import pandas as pd
from pyhocon import ConfigFactory
import time
import os
from loguru import logger
import time
from datetime import date , datetime, timedelta
import sqlalchemy as sql
import json
import psycopg2
import paramiko
import numpy as np
import json
from pandas_profiling import ProfileReport
from dateutil import parser
from fastapi.responses import FileResponse
import os
from fastapi.responses import HTMLResponse
from pandas_summary import DataFrameSummary


# Répertoire pour enregistrer les fichiers HTML générés
report_directory = '.'



def filtrer_dataframe_advanced(df, *args):
    filter_series = pd.Series(True, index=df.index)
    for arg in args:
        key = list(arg.keys())[0]  # Obtenir le nom de la colonne
        conditions = arg[key]  # Obtenir la liste des conditions pour cette colonne

        for condition in conditions:
            op = condition.get('op', 'eq')  # Par défaut, utilise l'opérateur '==' (égal)
            value = condition.get('value', None)  # Valeur de comparaison

            if op == 'eq':
                filter_series &= (df[key] == value)
            elif op == 'ne':
                filter_series &= (df[key] != value)
            elif op == 'lt':
                filter_series &= (df[key] < value)
            elif op == 'le':
                filter_series &= (df[key] <= value)
            elif op == 'gt':
                filter_series &= (df[key] > value)
            elif op == 'ge':
                filter_series &= (df[key] >= value)

    return df.loc[filter_series]

#Lecture d'un PAN csv
def read_pan_csv(date_debut):
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date_debut.strftime('%Y%m%d')  
    df=pd.read_csv(f'input/2022/pan_{formatted_date}.csv',sep=';')
    return df



############################################################################ Foyers ###################################################################################

#Age et Classe Age Panel
def get_age_foyers(df,date):
    #df=pd.DataFrame(read_pan_csv(date))
    #date = datetime.strptime(date, '%Y%m%d')
    
    formatted_date = date.strftime('%Y%m%d') 
    df['Age_Panel'] = df['Date_Installation'].apply(lambda x: int((parser.parse(formatted_date) - parser.parse(str(x))).days / 365))
    age_ranges = [(0, 1), (1, 2), (2, 3), (3, 4), (4, 5), (5, 6),(6, 7)]
    result = pd.DataFrame()
    for age_range in age_ranges:
        df_age = filtrer_dataframe_advanced(df, {'Age_Panel': [{'op': 'ge', 'value': age_range[0]}, {'op': 'lt', 'value': age_range[1]}]})
        df_age['Classe_Age_Panel'] = f"[{age_range[0]},{age_range[1]}["
        result = pd.concat([result, df_age], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]

    return result


#CSP Chef Menage
def get_csp_chef_menage(df,date):

    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    csp_values = [1,2,3,4]
    csp_ranges = ['CSP supérieur','CSP moyenne','CSP populaire','CSP rurale']
    result = pd.DataFrame()
    for value_csp,range_csp in zip(csp_values,csp_ranges):
        df_csp = filtrer_dataframe_advanced(df, {'F_CSP_CHEF': [{'op': 'eq', 'value': value_csp}]})
        df_csp['Classe_CSP_Chef_Menage'] = range_csp
        result = pd.concat([result, df_csp], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

#Lieu de residence
def get_lieu_residence(df,date):

    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    lieu_values = [1,2,3,4]
    lieu_ranges = ['Nord','Atlantique','Centre','Sud']
    result = pd.DataFrame()
    for value_lieu,range_lieu in zip(lieu_values,lieu_ranges):
        df_lieu = filtrer_dataframe_advanced(df, {'F_LIEURESIDENCE': [{'op': 'eq', 'value': value_lieu}]})
        df_lieu['Classe_Lieu_Residence'] = range_lieu
        result = pd.concat([result, df_lieu], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

#Milieu d'habitation
def get_milieu_habitation(df,date):

    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    milieu_values = [1,2]
    milieu_ranges = ['Urbain','Rural']
    result = pd.DataFrame()
    for value_milieu,range_milieu in zip(milieu_values,milieu_ranges):
        df_milieu = filtrer_dataframe_advanced(df, {'F_HABITATION': [{'op': 'eq', 'value': value_milieu}]})
        df_milieu['Classe_Milieu_Habitation'] = range_milieu
        result = pd.concat([result, df_milieu], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


## Lieu de residence x milieu d'habitation
def get_lieu_residence_x_milieu_habitation(df,date):

    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    milieu_values = [1,2,3,4,5,6,7,8]
    milieu_ranges = ['Nord Urbain','Nord Rural','Atlantique Urbain','Atlantique Rural','Centre Urbain','Centre Rural','Sud Urbain','Sud Rural']
    result = pd.DataFrame()
    for value_milieu,range_milieu in zip(milieu_values,milieu_ranges):
        df_milieu = filtrer_dataframe_advanced(df, {'F_LIEURESIDENCExHABITATION': [{'op': 'eq', 'value': value_milieu}]})
        df_milieu['Classe_Lieu_Residence_x_Habitation'] = range_milieu
        result = pd.concat([result, df_milieu], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result 

## Type d'offre
def get_type_offre(df,date):

    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    offre_values = [1,2]
    offre_ranges = ['Elargie','Restreinte']
    result = pd.DataFrame()
    for value_offre,range_offre in zip(offre_values,offre_ranges):
        df_offre = filtrer_dataframe_advanced(df, {'F_Type_offre': [{'op': 'eq', 'value': value_offre}]})
        df_offre['Classe_Type_Offre'] = range_offre
        result = pd.concat([result, df_offre], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


## CSP Foyer
def get_csp_foyer(df,date):

    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    csp_values = [1,2,3,4,5,6]
    csp_ranges = ['CSP A ','CSP B','CSP C','CSP D' ,'CSP E','CSP Rurale']
    result = pd.DataFrame()
    for value_csp,range_csp in zip(csp_values,csp_ranges):
        df_csp = filtrer_dataframe_advanced(df, {'F_CSPfoy': [{'op': 'eq', 'value': value_csp}]})
        df_csp['Classe_CSP_Foyer'] = range_csp
        result = pd.concat([result, df_csp], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Type d'offre x milieu
def get_type_offre_x_milieu(df,date):

    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    offre_values = [1,2,3,4]
    offre_ranges = ['Elargie Urbain ','Restreinte Urbain','Elargie Rural','Restreinte Rural']
    result = pd.DataFrame()
    for value_offre,range_offre in zip(offre_values,offre_ranges):
        df_offre = filtrer_dataframe_advanced(df, {'F_Type_offre_Milieu': [{'op': 'eq', 'value': value_offre}]})
        df_offre['Classe_Type_Offre_x_Milieu'] = range_offre
        result = pd.concat([result, df_offre], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Age Chef Menage
def get_age_chef_menage(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    age_values=[1,2]
    age_ranges = [(15, 34), (35, 44)]
    result = pd.DataFrame()
    
    for age_value,age_range in zip(age_values,age_ranges):
        df_age = filtrer_dataframe_advanced(df, {'F_Age_Chef_Menage': [{'op': 'eq', 'value': age_value}]})
        df_age['Classe_Age_Chef_Menage'] = f"{age_range[0]}-{age_range[1]}"
        result = pd.concat([result, df_age], ignore_index=True)
        
    res=filtrer_dataframe_advanced(df, {'F_Age_Chef_Menage': [{'op': 'ge', 'value': 4}]})
    res['Classe_Age_Chef_Menage'] = '45+'
    result = pd.concat([result, res], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage','Classe_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


## Nombre de personne dans un foyer
def get_nombre_personnes_foyer(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    Nbr_pers=[1,2,3]
    Nbr_pers_ranges = [(1, 3), (4, 5)]
    result = pd.DataFrame()
    
    for Nbr_pers_value,Nbr_pers_range in zip(Nbr_pers,Nbr_pers_ranges):
        df_Nbr_pers = filtrer_dataframe_advanced(df, {'F_NPERF': [{'op': 'eq', 'value': Nbr_pers_value}]})
        df_Nbr_pers['Classe_Nb_Pers_Foyer'] = f"{Nbr_pers_range[0]}-{Nbr_pers_range[1]} pers"
        result = pd.concat([result, df_Nbr_pers], ignore_index=True)
        
    res=filtrer_dataframe_advanced(df, {'F_NPERF': [{'op': 'eq', 'value': 3}]})
    res['Classe_Nb_Pers_Foyer'] = '6 pers et +'
    result = pd.concat([result, res], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF','Classe_Nb_Pers_Foyer', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Presence d'enfants dans un foyer
def get_presence_enfants_foyer(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    Pres_enfants=[1,2]
    Pres_enfants_mod = ['oui','non']
    result = pd.DataFrame()
    
    for value,mod in zip(Pres_enfants,Pres_enfants_mod):
        df_Pres_Enf = filtrer_dataframe_advanced(df, {'F_Presence_Enfants_Foyer': [{'op': 'eq', 'value': value}]})
        df_Pres_Enf['Classe_Presence_Enfants'] = f"{mod}"
        result = pd.concat([result, df_Pres_Enf], ignore_index=True)
        
    
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer','Classe_Presence_Enfants', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


## Activité Menagere Hors Foyer
def get_activite_menagere_hors_foyer(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    activite_value=[1,2,3]
    activite_mod = ['oui','non','sans épouse']
    result = pd.DataFrame()
    
    for value,mod in zip(activite_value,activite_mod):
        df_Activite_Menagere = filtrer_dataframe_advanced(df, {'F_Activite_Menagere_Hors_Foyer': [{'op': 'eq', 'value': value}]})
        df_Activite_Menagere['Classe_Activite_Menagere_Hors_Foyer'] = f"{mod}"
        result = pd.concat([result, df_Activite_Menagere], ignore_index=True)
        
    
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer','Classe_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Niveau Diplome Chef Menage
def get_niveau_diplome_chef_menage(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    niveau_value=[1,2,3]
    niveau_mod = ['pas de diplome','niveau moyen','niveau supérieur']
    result = pd.DataFrame()
    
    for value,mod in zip(niveau_value,niveau_mod):
        df_Niveau_Diplome_CM = filtrer_dataframe_advanced(df, {'F_Niveau_DiplomeCM': [{'op': 'eq', 'value': value}]})
        df_Niveau_Diplome_CM['Classe_Niveau_Diplome_CM'] = f"{mod}"
        result = pd.concat([result, df_Niveau_Diplome_CM], ignore_index=True)
        
    
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM','Classe_Niveau_Diplome_CM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Langue Parlée dans un Foyer
def get_langue_parlee_foyer(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    langue_value=[1,2]
    langue_mod = ['arabe','amazigh']
    result = pd.DataFrame()
    
    for value,mod in zip(langue_value,langue_mod):
        df_Langue_Parlee_Foyer = filtrer_dataframe_advanced(df, {'F_Langue_Parlee': [{'op': 'eq', 'value': value}]})
        df_Langue_Parlee_Foyer['Classe_Langue_Parlee_Foyer'] = f"{mod}"
        result = pd.concat([result, df_Langue_Parlee_Foyer], ignore_index=True)
        
    
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee','Classe_Langue_Parlee_Foyer', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Taille Commune Milieu 
def get_taille_commune_milieu(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    taille_commune_value=[1,2,3,4]
    taille_commune_mod = ['grande commune urbaine','moyenne commune urbaine','petite commune urbaine','commune rurales']
    result = pd.DataFrame()
    
    for value,mod in zip(taille_commune_value,taille_commune_mod):
        df_Taille_Commune_Milieu = filtrer_dataframe_advanced(df, {'F_Taille_Commune_Milieu': [{'op': 'eq', 'value': value}]})
        df_Taille_Commune_Milieu['Classe_Taille_Commune_Milieu'] = f"{mod}"
        result = pd.concat([result, df_Taille_Commune_Milieu], ignore_index=True)
        
    
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu','Classe_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Nombre TV par Foyer
def get_nombre_tv_foyer(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    nbr_tv_value=[1,2]
    nbr_tv_mod = ['1','2+']
    result = pd.DataFrame()
    
    for value,mod in zip(nbr_tv_value,nbr_tv_mod):
        df_Nbr_TV_Foyer = filtrer_dataframe_advanced(df, {'F_Nb_TV_Foyer': [{'op': 'eq', 'value': value}]})
        df_Nbr_TV_Foyer['Classe_Nbr_Tv_Foyer'] = f"{mod}"
        result = pd.concat([result, df_Nbr_TV_Foyer], ignore_index=True)
        
    
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer','Classe_Nbr_Tv_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

## Regions des foyers
def get_region_foyer(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    region_value=[1,2,3,4,5,6,7,8,9,10]
    region_mod = ['tanger-tétouan-al hoceima','oriental','fes-meknes','rabat-salé-kénitra','beni mellal-khénifra','grand casablanca-settat','marrakech-safi','draa-tafilalet','souss-massa','guelmim-laayoune-eddakhla']
    result = pd.DataFrame()
    
    for value,mod in zip(region_value,region_mod):
        df_Region_Foyer = filtrer_dataframe_advanced(df, {'F_REGION': [{'op': 'eq', 'value': value}]})
        df_Region_Foyer['Classe_Region_Foyer'] = f"{mod}"
        result = pd.concat([result, df_Region_Foyer], ignore_index=True)
        
    
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION','Classe_Region_Foyer', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer','F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


## Poids Foyers
def get_poids_foyer(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    df['Nv_Poids_Foyer'] = df['Poids_Foyer'].apply(lambda x: x/1000)
    poids_ranges = [(0.3, 0.5), (0.5, 0.7), (0.7, 0.9), (0.9, 1.1), (1.1, 1.3), (1.3, 1.5),(1.5, 1.7),(1.7,1.9),(1.9,2.1),(2.1,2.3),(2.3,2.5),(2.5,2.7),(2.7,2.9),(2.9,3.1),(3.1,3.3),(3.3,3.5),(3.5,3.7),(3.7,3.9),(3.9,4.1)]
    result = pd.DataFrame()
    for poids_range in poids_ranges:
        df_poids_foyer = filtrer_dataframe_advanced(df, {'Nv_Poids_Foyer': [{'op': 'ge', 'value': poids_range[0]}, {'op': 'lt', 'value': poids_range[1]}]})
        df_poids_foyer['Nv_Poids_Foyer'] = f"{poids_range[0]}-{poids_range[1]}"
        result = pd.concat([result, df_poids_foyer], ignore_index=True)
    df_poids_foyer = filtrer_dataframe_advanced(df, {'Nv_Poids_Foyer': [{'op': 'ge', 'value': 4.1}]})
    df_poids_foyer['Nv_Poids_Foyer'] = "+4.1"
    result = pd.concat([result, df_poids_foyer], ignore_index=True)
    df_poids_foyer = filtrer_dataframe_advanced(df, {'Nv_Poids_Foyer': [{'op': 'gt', 'value': 0}, {'op': 'lt', 'value': 0.3}]})

    df_poids_foyer['Nv_Poids_Foyer'] = "-0.3"
    result = pd.concat([result, df_poids_foyer], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

def get_all_infos_foyers(date):
    df=read_pan_csv(date)
    res1=get_age_foyers(df,date)
    res2=get_age_chef_menage(res1,date)
    res3=get_nombre_personnes_foyer(res2,date)
    res4=get_presence_enfants_foyer(res3,date)
    res5=get_activite_menagere_hors_foyer(res4,date)
    res6=get_niveau_diplome_chef_menage(res5,date)
    res7=get_langue_parlee_foyer(res6,date)
    res8=get_taille_commune_milieu(res7,date)
    res9=get_nombre_tv_foyer(res8,date)
    res10=get_region_foyer(res9,date)
    res11=get_poids_foyer(res10,date)
    res12=get_csp_chef_menage(res11,date)
    res13=get_lieu_residence(res12,date)
    res14=get_milieu_habitation(res13,date)
    res15=get_lieu_residence_x_milieu_habitation(res14,date)
    res16=get_type_offre(res15,date)
    res17=get_csp_foyer(res16,date)
    res18=get_type_offre_x_milieu(res17,date)
    res18=res18.drop_duplicates(['Foyer_ID'])
    df=res18.to_dict("records")
    return df


def get_summary_foyers(date):
    df=pd.DataFrame(get_all_infos_foyers(date))
    df=df.reset_index(drop=True)
    df=df[['Classe_CSP_Chef_Menage','Nv_Poids_Foyer','Classe_Age_Panel','Classe_Age_Chef_Menage','Classe_Nb_Pers_Foyer','Classe_Presence_Enfants','Classe_Activite_Menagere_Hors_Foyer','Classe_Niveau_Diplome_CM','Classe_Langue_Parlee_Foyer','Classe_Taille_Commune_Milieu','Classe_Nbr_Tv_Foyer','Classe_Region_Foyer','Classe_Lieu_Residence','Classe_Milieu_Habitation','Classe_Lieu_Residence_x_Habitation','Classe_Type_Offre','Classe_CSP_Foyer','Classe_Type_Offre_x_Milieu']]
    summary = DataFrameSummary(df)
    res_df=summary.summary()
    res_df=res_df.to_dict('records')
    return res_df



def get_report_foyers(date_debut):
    df=pd.DataFrame(get_all_infos_foyers(date_debut))
    df=df[['Classe_CSP_Chef_Menage','Nv_Poids_Foyer','Classe_Age_Panel','Classe_Age_Chef_Menage','Classe_Nb_Pers_Foyer','Classe_Presence_Enfants','Classe_Activite_Menagere_Hors_Foyer','Classe_Niveau_Diplome_CM','Classe_Langue_Parlee_Foyer','Classe_Taille_Commune_Milieu','Classe_Nbr_Tv_Foyer','Classe_Region_Foyer','Classe_Lieu_Residence','Classe_Milieu_Habitation','Classe_Lieu_Residence_x_Habitation','Classe_Type_Offre','Classe_CSP_Foyer','Classe_Type_Offre_x_Milieu']]
    profile = ProfileReport(df)
    # Enregistrez le rapport généré en tant que fichier HTML
    report_path = os.path.join(report_directory, "report.html")
    profile.to_file(report_path)
    # Enregistrez le rapport généré en tant que fichier HTML
    return FileResponse(report_path, headers={"Content-Disposition": "inline; filename=report.html"})



def generate_report_foyers(date_debut):
        df = pd.DataFrame(get_all_infos_foyers(date_debut.date_debut))
        df = df[['Classe_CSP_Chef_Menage','Nv_Poids_Foyer','Classe_Age_Panel','Classe_Age_Chef_Menage','Classe_Nb_Pers_Foyer','Classe_Presence_Enfants','Classe_Activite_Menagere_Hors_Foyer','Classe_Niveau_Diplome_CM','Classe_Langue_Parlee_Foyer','Classe_Taille_Commune_Milieu','Classe_Nbr_Tv_Foyer','Classe_Region_Foyer','Classe_Lieu_Residence','Classe_Milieu_Habitation','Classe_Lieu_Residence_x_Habitation','Classe_Type_Offre','Classe_CSP_Foyer','Classe_Type_Offre_x_Milieu']]
        profile = ProfileReport(df)
        
        # Generate the HTML content of the report
        report_content = profile.to_html()

        # Return the HTML content directly
        return HTMLResponse(content=report_content)
   


#################################################################### Individus ###################################################################################
def get_tranche_age_individus(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    age_values = [1,2,3,4,5,6]
    age_ranges = [('5','14'),('15','24'),('25','34'),('35','44'),('45','54'),('55','55+')]
    result = pd.DataFrame()
    for age_value,age_range in zip(age_values,age_ranges):
        df_age = filtrer_dataframe_advanced(df, {'I_AGE': [{'op': 'eq', 'value': age_value}]})
        df_age['Classe_Tranche_Age_Indiv'] = f"{age_range[0]}-{age_range[1]} ans"
        result = pd.concat([result, df_age], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


def get_age_croise_sexe(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    age_values = [1,2,3,4,5,6]
    age_ranges = ['Homme 5-14 ans','Femme 5-14 ans','Homme 15-34 ans','Femme 15-34 ans','Homme 35 ans et plus','Femme 35 ans et plus']
    result = pd.DataFrame()
    for age_value,age_range in zip(age_values,age_ranges):
        df_age = filtrer_dataframe_advanced(df, {'I_SEXEXAGE': [{'op': 'eq', 'value': age_value}]})
        df_age['Classe_Age_Croise_Sexe'] = age_range
        result = pd.concat([result, df_age], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result

def get_activite_individus(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    activite_values = [1,2]
    activite_ranges = ['Actifs','Inactifs']
    result = pd.DataFrame()
    for activite_value,activite_range in zip(activite_values,activite_ranges):
        df_activite = filtrer_dataframe_advanced(df, {'I_ACTIF': [{'op': 'eq', 'value': activite_value}]})
        df_activite['Classe_Activite_Individus'] = activite_range
        result = pd.concat([result, df_activite], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


def get_sexe_croise_activite(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    activite_values = [1,2,3,4]
    activite_ranges = ['Homme Actif','Femme Active','Homme Inactif','Femme Inactive']
    result = pd.DataFrame()
    for activite_value,activite_range in zip(activite_values,activite_ranges):
        df_activite = filtrer_dataframe_advanced(df, {'I_SEXEXACTIFS': [{'op': 'eq', 'value': activite_value}]})
        df_activite['Classe_Sexe_Croise_Activite'] = activite_range
        result = pd.concat([result, df_activite], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


def get_niveau_instruction(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    instruction_values = [1,2]
    instruction_ranges = ['Instruits','Non Instruits']
    result = pd.DataFrame()
    for instruction_value,instruction_range in zip(instruction_values,instruction_ranges):
        df_instruction = filtrer_dataframe_advanced(df, {'I_INSTRUCTION': [{'op': 'eq', 'value': instruction_value}]})
        df_instruction['Classe_Niveau_Instruction'] = instruction_range
        result = pd.concat([result, df_instruction], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


def get_sexe_croise_niveau_instruction(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    instruction_values = [1,2,3,4]
    instruction_ranges = ['Homme Instruit','Femme Instruite','Homme Non Instruit','Femme Non Instruite']
    result = pd.DataFrame()
    for instruction_value,instruction_range in zip(instruction_values,instruction_ranges):
        df_instruction = filtrer_dataframe_advanced(df, {'I_SEXE_INSTRUCTION': [{'op': 'eq', 'value': instruction_value}]})
        df_instruction['Classe_Sexe_Croise_Niveau_Instruction'] = instruction_range
        result = pd.concat([result, df_instruction], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


def get_niveau_diplome(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    diplome_values = [1,2,3]
    diplome_ranges = ['Sans diplome','Niveau moyen','Niveau superieur']
    result = pd.DataFrame()
    for diplome_value,diplome_range in zip(diplome_values,diplome_ranges):
        df_diplome = filtrer_dataframe_advanced(df, {'I_Niveau_Diplome': [{'op': 'eq', 'value': diplome_value}]})
        df_diplome['Classe_Niveau_Diplome'] = diplome_range
        result = pd.concat([result, df_diplome], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


def get_poids_individu(df,date):
    #df=read_pan_csv(date)
    #date = datetime.strptime(date, '%Y%m%d')
    formatted_date = date.strftime('%Y%m%d') 
    df['Nv_Poids_Individu'] = df['Poids_Individu'].apply(lambda x: x/1000)
    poids_ranges = [(0.3, 0.5), (0.5, 0.7), (0.7, 0.9), (0.9, 1.1), (1.1, 1.3), (1.3, 1.5),(1.5, 1.7),(1.7,1.9),(1.9,2.1),(2.1,2.3),(2.3,2.5),(2.5,2.7),(2.7,2.9),(2.9,3.1),(3.1,3.3),(3.3,3.5),(3.5,3.7),(3.7,3.9),(3.9,4.1)]
    result = pd.DataFrame()
    for poids_range in poids_ranges:
        df_poids_individu = filtrer_dataframe_advanced(df, {'Nv_Poids_Individu': [{'op': 'ge', 'value': poids_range[0]}, {'op': 'lt', 'value': poids_range[1]}]})
        df_poids_individu['Nv_Poids_Individu'] = f"{poids_range[0]}-{poids_range[1]}"
        result = pd.concat([result, df_poids_individu], ignore_index=True)
    df_poids_foyer = filtrer_dataframe_advanced(df, {'Nv_Poids_Individu': [{'op': 'ge', 'value': 4.1}]})
    df_poids_foyer['Nv_Poids_Individu'] = "+4.1"
    result = pd.concat([result, df_poids_foyer], ignore_index=True)
    df_poids_foyer = filtrer_dataframe_advanced(df, {'Nv_Poids_Individu': [{'op': 'gt', 'value': 0}, {'op': 'lt', 'value': 0.3}]})

    df_poids_foyer['Nv_Poids_Individu'] = "-0.3"
    result = pd.concat([result, df_poids_foyer], ignore_index=True)
    #new_order = ['DatePan', 'Foyer_ID', 'Date_Installation', 'Age_Panel', 'Classe_Age_Panel','Poids_Foyer', 'individu_ID', 'Poids_Individu', 'F_NPERF', 'F_CSP_CHEF', 'F_LIEURESIDENCE', 'F_HABITATION', 'F_LIEURESIDENCExHABITATION', 'F_Type_offre', 'F_CSPfoy', 'F_Type_offre_Milieu', 'F_REGION', 'F_Age_Chef_Menage', 'F_Presence_Enfants_Foyer', 'F_Activite_Menagere_Hors_Foyer', 'F_Langue_Parlee', 'F_Nb_TV_Foyer', 'F_Niveau_DiplomeCM', 'F_Taille_Commune_Milieu', 'I_AGE', 'I_AGE1', 'I_AGE2', 'I_AGE4', 'I_SEXEXAGE', 'I_SEXEXAGE3', 'I_ACTIF', 'I_SEXEXACTIFS', 'I_INSTRUCTION', 'I_SEXE_INSTRUCTION', 'I_AGE_CLAIR', 'I_Niveau_Diplome']
    #result = result[new_order]
    return result


def get_all_infos_individus(date):
    df=read_pan_csv(date)
    res1=get_tranche_age_individus(df,date)
    res2=get_age_croise_sexe(res1,date) 
    res3=get_activite_individus(res2,date)
    res4=get_sexe_croise_activite(res3,date)
    res5=get_niveau_instruction(res4,date)
    res6=get_sexe_croise_niveau_instruction(res5,date)
    res7=get_niveau_diplome(res6,date)
    res8=get_poids_individu(res7,date) 
    res8=res8.to_dict('records') 
    return res8

def get_summary_individus(date):
    df=pd.DataFrame(get_all_infos_individus(date))
    df=df[['Classe_Tranche_Age_Indiv','Classe_Age_Croise_Sexe','Classe_Activite_Individus','Classe_Sexe_Croise_Activite','Classe_Niveau_Instruction','Classe_Sexe_Croise_Niveau_Instruction','Classe_Niveau_Diplome','Nv_Poids_Individu']]
    summary = DataFrameSummary(df)
    res_df=summary.summary()
    res_df=res_df.to_dict('records')
    return res_df





def get_report_individus(date_debut):
    df=pd.DataFrame(get_all_infos_individus(date_debut))
    df=df[['Classe_Tranche_Age_Indiv','Classe_Age_Croise_Sexe','Classe_Activite_Individus','Classe_Sexe_Croise_Activite','Classe_Niveau_Instruction','Classe_Sexe_Croise_Niveau_Instruction','Classe_Niveau_Diplome','Nv_Poids_Individu']]
    profile = ProfileReport(df)
    # Enregistrez le rapport généré en tant que fichier HTML
    report_path = os.path.join(report_directory, "report.html")
    profile.to_file(report_path)
    # Enregistrez le rapport généré en tant que fichier HTML
    return FileResponse(report_path, headers={"Content-Disposition": "inline; filename=report.html"})



def generate_report_individus(date_debut):
        df = pd.DataFrame(get_all_infos_foyers(date_debut.date_debut))
        df=df[['Classe_Tranche_Age_Indiv','Classe_Age_Croise_Sexe','Classe_Activite_Individus','Classe_Sexe_Croise_Activite','Classe_Niveau_Instruction','Classe_Sexe_Croise_Niveau_Instruction','Classe_Niveau_Diplome','Nv_Poids_Individu']]
        profile = ProfileReport(df)
        
        # Generate the HTML content of the report
        report_content = profile.to_html()

        # Return the HTML content directly
        return HTMLResponse(content=report_content)
